package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class IntegrationTopDownTest {

    private RepositoryActivity repAct;
    private RepositoryContact repCon;

    @Before
    public void setup() throws Exception {
        repCon = new RepositoryContactFile();
        repAct = new RepositoryActivityFile(repCon);

        for (Activity a : repAct.getActivities())
            repAct.removeActivity(a);
    }

    @Test
    public void test1() {
        Contact con = null;
        int n = repCon.count();
        try {
            con = new Contact("Iulian", "Cluj", "+0748454948","udisteanu.iulian@outlook.com");
            repCon.addContact(con);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        }


        Contact contact = repCon.getByName("Iulian");
        assertTrue(contact.getEmail().equals("udisteanu.iulian@outlook.com"));
        assertTrue(repCon.count()==n+1);
    }

    @Test
    public void test2() {
        Contact con = null;
        Activity act = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        int n = repCon.count();
        try {
            con = new Contact("Iulian", "Cluj", "+978766554","udisteanu.iulian@outlook.com");
            repCon.addContact(con);
            act = new Activity(con.getName(), df.parse("04/20/2018 12:00"),
                    df.parse("04/20/2018 13:00"), null, "Lunch break");
            repAct.addActivity(act);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Contact contact = repCon.getByName("Iulian");
        assertTrue(contact.getEmail().equals("udisteanu.iulian@outlook.com"));
        assertTrue(repCon.count()==n+1);
        assertTrue(repAct.getActivities().get(0).equals(act)
                && repAct.count() == 1);


    }

    @Test
    public void test3() {
        Contact con = null;
        Activity act = null;
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        int n = repCon.count();
        try {
            con = new Contact("Iulian", "Cluj", "+978766554","udisteanu.iulian@outlook.com");
            repCon.addContact(con);
            act = new Activity(con.getName(), df.parse("04/20/2018 12:00"),
                    df.parse("04/20/2018 13:00"), null, "Curs");
            repAct.addActivity(act);
        } catch (InvalidFormatException e) {
            assertTrue(false);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Contact contact = repCon.getByName("Iulian");
        assertTrue(contact.getEmail().equals("udisteanu.iulian@outlook.com"));
        assertTrue(repCon.count()==n+1);
        assertTrue(repAct.getActivities().get(0).equals(act)
                && repAct.count() == 1);


        Calendar c = Calendar.getInstance();
        c.set(2018, 4-1, 20);

        List<Activity> result = repAct.activitiesOnDate(contact.getName(), c.getTime());
        assertTrue(result.size()==1 && result.get(0).equals(act));
        assertTrue(result.get(0).getDescription().equals("Curs"));
    }
}