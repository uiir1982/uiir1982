package agenda.test;

import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

public class IntegrationBigBangTest {

	private RepositoryActivity repAct;
	private RepositoryContact repCon;

	@Before
	public void setup() throws Exception {
		repCon = new RepositoryContactFile();
		repAct = new RepositoryActivityFile(repCon);

		for (Activity a : repAct.getActivities())
			repAct.removeActivity(a);
	}

	@Test
	public void test1() {
		int n = repCon.count();

		try {
			Contact c = new Contact("name", "address1", "+071122334455","email@yahoo.com");
			repCon.addContact(c);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}

		assertTrue(n + 1 == repCon.count());
	}

	@Test
	public void test2() {
		Activity act = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("name", df.parse("03/20/2013 12:00"),
					df.parse("03/20/2013 13:00"), null, "Lunch break");
			repAct.addActivity(act);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertTrue(repAct.getActivities().get(0).equals(act)
				&& repAct.count() == 1);
	}

	@Test
	public void test3() {
		Calendar c = Calendar.getInstance();
		c.set(2018, 04, 20, 12, 00);
		Date start = c.getTime();

		c.set(2018, 04, 20, 12, 30);
		Date end = c.getTime();

		Activity act = new Activity("name", start, end,
				new LinkedList<Contact>(), "description2");

		repAct.addActivity(act);

		c.set(2018, 04, 20);

		List<Activity> result = repAct.activitiesOnDate("name", c.getTime());
		assertTrue(result.size() == 1);
		assertTrue(result.get(0).getDescription().equals("description2"));
	}

	@Test
	public void test4() {
		Activity act = null;
		Contact contact = null;
		int n = repCon.count();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			contact = new Contact("Julian","adresa1","+0748454948","udisteanu.iulian@outlook.com");
			repCon.addContact(contact);
			act = new Activity(contact.getName(), df.parse("04/20/2018 12:00"),
					df.parse("04/20/2018 13:00"), null, "Lunch break");
			repAct.addActivity(act);
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		assertTrue(repCon.count()==n+1);
		assertTrue(repAct.getActivities().get(0).equals(act)
				&& repAct.count() == 1);

		Calendar c = Calendar.getInstance();
		c.set(2018, 4-1, 20);

		List<Activity> result = repAct.activitiesOnDate(contact.getName(), c.getTime());
		assertTrue(result.size()==1 && result.get(0).equals(act));
		assertTrue(result.get(0).getDescription().equals("Lunch break"));
	}



}
