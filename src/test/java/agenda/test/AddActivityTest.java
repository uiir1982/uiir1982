package agenda.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import agenda.model.base.Activity;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.interfaces.RepositoryActivity;

import org.junit.Before;
import org.junit.Test;

public class AddActivityTest {
	private Activity act;
	private RepositoryActivity rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryActivityMock();
	}
	
	@Test
	public void testCase1()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("name1", 
					df.parse("04/27/2018 08:00"),
					df.parse("04/27/2018 10:00"),
					null,
					"Curs Istoria informaticii");
			//rep.addActivity(act);
			assertTrue(rep.addActivity(act));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertTrue(1 == rep.count());
	}
	
	@Test
	public void testCase2()
	{
		Activity act1;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		try {
			act = new Activity("name1",
                    df.parse("04/27/2018 08:00"),
                    df.parse("04/27/2018 10:00"),
                    null,
                    "Curs Istoria informaticii");

			act1 =new Activity("name1",
					df.parse("04/27/2018 10:00"),
					df.parse("04/27/2018 12:00"),
					null,
					"Curs Istoria informaticii");

			assertTrue(rep.addActivity(act));
			assertTrue(rep.addActivity(act1));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		assertTrue(rep.count()==2);

	}
	
	@Test
	public void testCase3()
	{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Activity act1;
		try{

			act = new Activity("name1",
					df.parse("04/27/2018 08:00"),
					df.parse("04/27/2018 10:00"),
					null,
					"Curs Istoria informaticii");
			assertTrue(rep.addActivity(act));
			
			act1 = new Activity("name1",
					df.parse("04/20/2018 09:30"),
					df.parse("04/20/2018 13:30"),
					null,
					"Lunch break");
			assertFalse(rep.addActivity(act));
		}
		catch(Exception e){

		}

		assertTrue(rep.count()==1);
	}

}
