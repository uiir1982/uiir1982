package agenda.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;

import org.junit.Before;
import org.junit.Test;

public class AfisActivityTest {

	RepositoryActivity rep;

	@Before
	public void setUp() throws Exception {
		RepositoryContact repcon = new RepositoryContactFile();
		rep = new RepositoryActivityFile(repcon);

		for (Activity act : rep.getActivities())
			rep.removeActivity(act);
	}

	@Test
	public void testCase1() {

		Calendar c = Calendar.getInstance();
		c.set(2018, 04, 20, 12, 00);
		Date start = c.getTime();

		c.set(2018, 04, 20, 12, 30);
		Date end = c.getTime();

		Activity act = new Activity("namel", start, end,
				new LinkedList<Contact>(), "description2");

		rep.addActivity(act);

		c.set(2018, 04, 20);

		List<Activity> result = rep.activitiesOnDate("namel", c.getTime());
		assertTrue(result.size() == 1);
	}

	@Test
	public void testCase2() {

		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20, 15, 00);
		Date start = c.getTime();

		c.set(2013, 3 - 1, 20, 15, 30);
		Date end = c.getTime();

		Activity act = new Activity("namel", start, end,
				new LinkedList<Contact>(), "description3");

		rep.addActivity(act);

		c.set(2013, 3 - 1, 20, 13, 00);
		Date start1 = c.getTime();

		c.set(2013, 3 - 1, 20, 13, 30);
		Date end1 = c.getTime();

		Activity act1 = new Activity("namel", start1, end1,
				new LinkedList<Contact>(), "description1");

		rep.addActivity(act1);

		c.set(2013, 3 - 1, 20);
		List<Activity> result = rep.activitiesOnDate("namel",c.getTime());
		assertTrue(result.size()==2);
		assertTrue(result.get(0).getDescription().equals("description1"));
		assertTrue(result.get(1).getDescription().equals("description3"));
	}
	
	@Test
	public void testCase3() {

		try {
			rep.activitiesOnDate("namel", (Date)(Object)"ASD");
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase4() {

		try {
			rep.addActivity((Activity)(Object)1);
			
			Calendar c = Calendar.getInstance();
			c.set(2013, 3 - 1, 20);
			rep.activitiesOnDate("namel", c.getTime());
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCase5() {
	
		Calendar c = Calendar.getInstance();
		c.set(2013, 3 - 1, 20);
		List<Activity> result = rep.activitiesOnDate("namel", c.getTime());
		
		assertTrue(result.size() == 0);
	}
}
