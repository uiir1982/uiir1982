package agenda.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	
	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void PrimulTest()
	{
		try {
			con = new Contact("Iulian", "Cluj", "+0748454948","udisteanu.iulian@outlook.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}


		Contact contact = rep.getByName("Iulian");
		assertTrue(contact.getEmail().equals("udisteanu.iulian@outlook.com"));
		assertTrue(rep.count()==1);
	}
	
	@Test
	public void AlDoileaTest()
	{
		try {
			con = new Contact("Iulian", "Cluj", "+978766554","udisteanu.iulian@outlook.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}


		Contact contact = rep.getByName("Iulian");
		assertTrue(contact.getEmail().equals("udisteanu.iulian@outlook.com"));
		assertTrue(rep.count()==1);
	}
	
	@Test
	public void testCase3()
	{
		try {
			con = new Contact("Iulian", "Cluj", "+978766554","dsafasd");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
			assertTrue(e.getCause().getMessage().equals("Invalid email"));
		}

		assertTrue(rep.count()==0);
	}

	@Test
	public void testCase4()
	{
		try {
			con = new Contact("Iulian", "Cluj", "53244","fdsg");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
			assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
		}

		assertTrue(rep.count()==0);
	}

	@Test
	public void testCase5()
	{
		try {
			con = new Contact("Iulian", "Cluj", "+0748454948","udisteanu.iulian@outlook.com");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
			assertTrue(e.getCause().getMessage().equals("Invalid phone number"));
		}

		assertTrue(rep.count()==1);
	}
	@Test
	public void testCase6()
	{
		try {
			con = new Contact("Ciprian", "Suceava", "+76444333555","ciprian@yahoo.com");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

		assertTrue(rep.count()==1);
	}
	@Test
	public void testCase7()
	{
		try {
			con = new Contact("Iuli9n", "Cluj", "+0748454948","udisteanu.iulian@outlook.com");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
			assertTrue(e.getCause().getMessage().equals("Invalid name"));

		}

		assertTrue(rep.count()==0);
	}
	@Test
	public void testCase8()
	{
		try {
			con = new Contact("Iulian", "Cluj", "+0748454948","");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
			assertTrue(e.getCause().getMessage().equals("Invalid email"));
		}

		assertTrue(rep.count()==0);
	}

	@Test
	public void testCase9()
	{
		try {
			con = new Contact("Iulian Elisei Udisteanu", "Cluj", "0748454948","udi@outlook.com");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
		}

		assertTrue(rep.count()==1);
	}

	@Test
	public void testCase10()
	{
		try {
			con = new Contact("Iulian", "", "0748454948","ceva@yahoo.ro");
			rep.addContact(con);

		} catch (InvalidFormatException e) {
			assertTrue(true);
			assertTrue(e.getCause().getMessage().equals("Invalid address"));
		}

		assertTrue(rep.count()==0);
	}
	
}
